﻿namespace FRC_IP_Manager
{
    partial class IPForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IPForm));
            this.RollbackButton = new System.Windows.Forms.Button();
            this.StaticButton = new System.Windows.Forms.Button();
            this.RollbackTip = new System.Windows.Forms.ToolTip(this.components);
            this.ProgressBar = new System.Windows.Forms.ProgressBar();
            this.StaticTip = new System.Windows.Forms.ToolTip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TeamNumberBox = new System.Windows.Forms.TextBox();
            this.EthernetConnection = new System.Windows.Forms.RadioButton();
            this.WifiConnection = new System.Windows.Forms.RadioButton();
            this.ImagingCheck = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // RollbackButton
            // 
            resources.ApplyResources(this.RollbackButton, "RollbackButton");
            this.RollbackButton.Name = "RollbackButton";
            this.RollbackButton.Tag = "Rollback Button";
            this.RollbackTip.SetToolTip(this.RollbackButton, resources.GetString("RollbackButton.ToolTip"));
            this.StaticTip.SetToolTip(this.RollbackButton, resources.GetString("RollbackButton.ToolTip1"));
            this.RollbackButton.UseVisualStyleBackColor = true;
            this.RollbackButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // StaticButton
            // 
            resources.ApplyResources(this.StaticButton, "StaticButton");
            this.StaticButton.Name = "StaticButton";
            this.StaticButton.Tag = "Static IP Button";
            this.RollbackTip.SetToolTip(this.StaticButton, resources.GetString("StaticButton.ToolTip"));
            this.StaticTip.SetToolTip(this.StaticButton, resources.GetString("StaticButton.ToolTip1"));
            this.StaticButton.UseVisualStyleBackColor = true;
            this.StaticButton.Click += new System.EventHandler(this.static_Click);
            // 
            // RollbackTip
            // 
            this.RollbackTip.ToolTipTitle = "Reset IP";
            // 
            // ProgressBar
            // 
            resources.ApplyResources(this.ProgressBar, "ProgressBar");
            this.ProgressBar.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Step = 20;
            this.RollbackTip.SetToolTip(this.ProgressBar, resources.GetString("ProgressBar.ToolTip"));
            this.StaticTip.SetToolTip(this.ProgressBar, resources.GetString("ProgressBar.ToolTip1"));
            // 
            // StaticTip
            // 
            this.StaticTip.ToolTipTitle = "Set Specific IP";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Name = "label1";
            this.RollbackTip.SetToolTip(this.label1, resources.GetString("label1.ToolTip"));
            this.StaticTip.SetToolTip(this.label1, resources.GetString("label1.ToolTip1"));
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            this.RollbackTip.SetToolTip(this.label2, resources.GetString("label2.ToolTip"));
            this.StaticTip.SetToolTip(this.label2, resources.GetString("label2.ToolTip1"));
            // 
            // TeamNumberBox
            // 
            resources.ApplyResources(this.TeamNumberBox, "TeamNumberBox");
            this.TeamNumberBox.Name = "TeamNumberBox";
            this.StaticTip.SetToolTip(this.TeamNumberBox, resources.GetString("TeamNumberBox.ToolTip"));
            this.RollbackTip.SetToolTip(this.TeamNumberBox, resources.GetString("TeamNumberBox.ToolTip1"));
            // 
            // EthernetConnection
            // 
            resources.ApplyResources(this.EthernetConnection, "EthernetConnection");
            this.EthernetConnection.Name = "EthernetConnection";
            this.EthernetConnection.TabStop = true;
            this.RollbackTip.SetToolTip(this.EthernetConnection, resources.GetString("EthernetConnection.ToolTip"));
            this.StaticTip.SetToolTip(this.EthernetConnection, resources.GetString("EthernetConnection.ToolTip1"));
            this.EthernetConnection.UseVisualStyleBackColor = true;
            // 
            // WifiConnection
            // 
            resources.ApplyResources(this.WifiConnection, "WifiConnection");
            this.WifiConnection.Name = "WifiConnection";
            this.WifiConnection.TabStop = true;
            this.RollbackTip.SetToolTip(this.WifiConnection, resources.GetString("WifiConnection.ToolTip"));
            this.StaticTip.SetToolTip(this.WifiConnection, resources.GetString("WifiConnection.ToolTip1"));
            this.WifiConnection.UseVisualStyleBackColor = true;
            // 
            // ImagingCheck
            // 
            resources.ApplyResources(this.ImagingCheck, "ImagingCheck");
            this.ImagingCheck.Name = "ImagingCheck";
            this.RollbackTip.SetToolTip(this.ImagingCheck, resources.GetString("ImagingCheck.ToolTip"));
            this.StaticTip.SetToolTip(this.ImagingCheck, resources.GetString("ImagingCheck.ToolTip1"));
            this.ImagingCheck.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            this.RollbackTip.SetToolTip(this.label3, resources.GetString("label3.ToolTip"));
            this.StaticTip.SetToolTip(this.label3, resources.GetString("label3.ToolTip1"));
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // IPForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ImagingCheck);
            this.Controls.Add(this.WifiConnection);
            this.Controls.Add(this.EthernetConnection);
            this.Controls.Add(this.TeamNumberBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ProgressBar);
            this.Controls.Add(this.StaticButton);
            this.Controls.Add(this.RollbackButton);
            this.Name = "IPForm";
            this.StaticTip.SetToolTip(this, resources.GetString("$this.ToolTip"));
            this.RollbackTip.SetToolTip(this, resources.GetString("$this.ToolTip1"));
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button RollbackButton;
        private System.Windows.Forms.Button StaticButton;
        private System.Windows.Forms.ToolTip RollbackTip;
        private System.Windows.Forms.ProgressBar ProgressBar;
        private System.Windows.Forms.ToolTip StaticTip;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TeamNumberBox;
        private System.Windows.Forms.RadioButton EthernetConnection;
        private System.Windows.Forms.RadioButton WifiConnection;
        private System.Windows.Forms.CheckBox ImagingCheck;
        private System.Windows.Forms.Label label3;
    }
}

